package onixred.golos.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.maksimov.andrey.commons.log.Loggable;

@RestController
public class СommonsController {

	@RequestMapping("/")
	@Loggable
    public String index() {
        return "golos";
    }
}
